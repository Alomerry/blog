---
article: false
description: App 介绍
date: 2022-04-26
---

# Apps Introduction

几次重装下来的干货，基本算是个人必备，仅供参考

## Jetbrains Tools

Jetbrains IDE 工具，可以统一下载更新旗下的 IDE、参与测试等

![jetbrains-tools-01](https://cdn.alomerry.com/blog/assets/img/posts/apps-intro-jetbrains-tools-01.jpg)

## Karabiner

## SoundSource

## alttab

## ucltter

## Slidepad

## bartender 4

## istats

## arc browser

## mos

## bark