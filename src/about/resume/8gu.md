---
enableFootnotePopup: true
date: 2023-07-20
timeline: false
article: false
---

# 八股文

<meter min="0" max="243" value="83" /> <strong> {{ Number(83/243*100).toFixed(2) }}% </strong> `(83/243)` <Badge text="Total" type="tip"/>

<meter min="0" max="109" value="41" /> <strong> {{ Number(41/109*100).toFixed(2) }}% </strong>  `(41/109)` <Badge text="极重要" type="danger"/>

<meter min="0" max="103" value="42" /> <strong> {{ Number(42/103*100).toFixed(2) }}% </strong>  `(42/103)` <Badge text="非常重要" type="warning"/>

<meter min="0" max="45" value="0" /> <strong> {{ Number(0/34*100).toFixed(2) }}% </strong> `(0/34)` <Badge text="重要（待定，先不准备）" type="info"/>

## Golang <Badge text="极重要/一" type="danger"/> `(41/109)`

### 结构 `(41/77)`

#### map `(15/15)` <Badge text="Done" type="tip"/>

- [x] map 在传参时的类型
  - map 传参时是值传递，传递的是 hmap 指针，在函数中操作 map 时，都是对 hmap 同一个 buckets 的操作，也因此调用函数修改 map 键值对时，会使 caller 中传递的 map 元素发生改变
- [x] Golang 如何做内存管理的，比如有一个 map，删除了其中某些 key，那么这部分内存如何释放
  - 对于 map，删除元素不会释放内存，只会将元素位置置空，需要使用：定期备份、value 使用指针类型（删除后通过 gc 回收）
- [x] Go 使用 Map 需要注意的点：有序性、扩容机制、多并发安全问题。
- [x] 讲讲哈希表处理冲突的方法，map 底层数据结构
  - 处理冲突的方式正常有两种：一种是开放寻址法，即产生冲突后将依次向后存储，访问时需要依次检查；另一种是拉链法，即产生冲突时在对应位置链上新元素，访问时在该位置遍历链表
  - 在 go 中将以上两种方式结合，宏观上更贴近拉链法。结合 map 底层结构 hmap 中单个桶结构 bmap 为例，当定位到某个桶时，会依次填充桶中的八个位置，当填充满了之后会链上新的溢出桶继续存储。
- [x] 你对 map 的理解？它是并发安全的吗？如果用原生 map 实现并发安全的话要怎么做
  - map 表达的事一种一对一的关系，是使用任意 key 通过哈希函数可以获得对应的 value
  - 不是并发安全的
  - 使用 `sync.Mutex`
- [x] map 并发安全吗？为什么
  - 不支持并发读写，会在 `mapassign` 和 `mapdelete` 中标记 flag 正在写入，在写入结束前和 `mapaccsee` 中都会验证 flag 写入位是否修改，修改则会 fatal error 终止整个程序
- [x] 是所有的 map 都不支持读写并发嘛？
  - `sync.map` 支持并发读写
- [x] go map 为什么要设计成不支持并发的
  - map 的典型使用场景是不需要从多个 goroutine 中进行安全访问，若是只是为少数程序增加安全性，导致 map 所有的操作都要处理 mutex，将会降低大多数程序的性能。
  - 并发导致 fatal error 基于 let it crash 思想
- [x] 并发读写崩溃原理
  - map 的底层结构 hmap 里有 `flag` 字段，第三位位写入位 `hashWriting`，标记当前 map 是否在写入，并发修改、修改时读取会产生 `fatal("concurrent map writes")` 错误
- [x] nil map 和空 map 区别
  - nil map 是不能赋值的，因为没有初始化 <Badge text="更详细" type="tip"/>
  - nil map 可以取值，但都是空的
- [x] 哪些能作为 map 的 key 哪些不能？
  - map 的 key 需要是可哈希的，`func`、`map`、`slice` 不支持
- [x] 使用 map 时候哪些情况会出现 panic
  - 读的过程中会有更新、删除操作时
  - 多个写操作时
- [x] 如果我们把一个 map 的 key 删除了，他的内存会释放吗？
  - 不会释放 <Badge text="详细" type="tip"/>
- [x] Go map 的扩容机制
  - go 中有两种扩容机制：一种是等量扩容，一种是翻倍扩容，解决了两种问题，当大量 key 计算出的哈希值都集中落到了某个桶时，或是桶链上了很多溢出桶，后续删除元素后，并不会合并溢出桶，会导致该桶链上了大量溢出桶，查找效率降低，退化成链表，此时需要等量扩容；当桶中的元素过多，元素数量和桶数量的比值超过 6.5 时，访问效率也会下降，此时需要翻倍扩容
  - 等量扩容会将对应桶的元素复制到新桶对应位置；翻倍扩容会根据 key 的哈希值将元素分流到新桶切片的两个对应位置
- [x] map 为什么遍历取出来的值顺序不一样
  - hmap 在初始化的时候会初始化一个随机数种子，遍历是会用通过种子去随机遍历 <Badge text="更详细" type="tip"/>

#### goroutine `(11/16)`

- [x] 协程操作系统是没有感知的，它怎么知道协程被切换了
  - 协程是在用户态进行切换的，操作系统无法感知
- [ ] golang 中使用 goroutine 使用系统调用会阻塞线程吗
  - https://qiankunli.github.io/2020/11/21/goroutine_system_call.html
- [x] goroutine 可能泄漏吗？gorotinue 通信方式
  - 可能泄露（无法正常退出）：协程永久阻塞无法唤醒（channel 收发阻塞）、死锁、无限循环
  - channel、ctx、全局共享变量
- [x] 为什么 golang 的协程快
  - 协程栈内存小，切换上下文更快
  - 协程切换处于用户空间
- [x] 协程适用什么场景
  - I/O 密集型、高并发
- [x] go 的 goroutine 共享栈吗
  - 不共享栈（分配 goroutine 或者协程栈增长时会统一切换到 g0 栈 <Badge text="TODO 证明" type="tip"/>）
- [x] 抢占式调度是如何抢占的
  - 插入栈增长代码
  - gc、系统监控中检查协程运行时间过长，修改 g 的标志位
  - 被抢占协程执行函数调用，检测到抢占标记，执行调度
- [ ] golang 三大件（内存 并发 GMP）讲一讲
- [ ] go 内存管理
  - https://draveness.me/golang/docs/part3-runtime/ch07-memory/golang-memory-allocator/
- [ ] 了解调度器吗？你的理解？
- [x] 限制 P 的个数的有哪些方案
  - 设置 GOMAXPROCS
  - 在容器中可以限制 CPU
- [x] 两个协程同时访问一个全局变量，使用什么方法使他们不会阻塞
  - 自旋、原子包操作、某些情况下读写锁
- [ ] 协程是怎么实现的（有自己的栈空间，协程 id 号，栈里有寄存器，当时第一次面试忘记说了 runtime 包中的 GMP 调度了）
- [x] 进程、线程和协程区别
  - 进程和线程的切换需要在内核态进行，受操作系统调度，协程由 scheld 在用户态调度
  - 进程线程的上下文内存大，协程需要的上下文很小
  - 进程的创建需要系统调用分配进程 ID、文件描述附表、进程地址空间、命名空间等；线程享有进程的文件描述符表、虚拟地址空间等，但是也拥有自己的内核数据结构，内核空间和用户空间
  - https://blog.csdn.net/qq_34147021/article/details/85654879?spm=1001.2014.3001.5506
- [x] 主协程下的子协程访问连接数据库访问失败一直循环，怎么通过主协程关闭它
  - cancel ctx、timeout ctx、channel+select
- [x] 如何控制并发数
  - 利用缓冲 channel
  - 利用锁控制
  - 协程池 ants

#### array/slice `(5/6)`

- [x] 字符串转成 byte 数组，会发生内存拷贝吗？
  - 字符串底层结构是 StringHeader[^StringHeader]，切片底层是 SliceHeader[^SliceHeader]，使用 `unsafe` 包转换底层结构可以不发生内存拷贝
- [x] 如何删除切片中的某一个元素
  - 截取、拷贝、移位
- [x] 切片的扩容机制 [growslice](https://github.com/golang/go/blob/202a1a57064127c3f19d96df57b9f9586145e21c/src/runtime/slice.go#L157)
  - 1.17 之前：小于 1024 会翻倍，超过 1024 会扩到 1.25
  - 1.18 之后：以256为临界点，超过 256，不再是每次扩容 1/4，[而是每次增加 `(old size+3*256)/4`](https://juejin.cn/post/7101928883280150558)
- [x] 数组和切片的区别
  - 数组需要声明长度，切片是动态数组，其长度并不固定，我们可以向切片中追加元素，它会在容量不足时自动扩容
- [ ] 你对 Slice 类型的理解
- [x] slice 数据结构，nil 切片和空切片
  - SliceHeader[^SliceHeader]
  - https://cloud.tencent.com/developer/article/1796710

#### 锁 `(5/7)`

- [ ] go 语言实现一个非阻塞锁
  - atomic？channel？
- [x] atomic 包哪里用到了
  - sync.mutex、sync.map
- [x] mutex 是悲观锁还是乐观锁？
  - 悲观锁
- [x] `sync.Mutex` 怎么使用
  - 使用 `Lock` 加锁，`UnLock` 解锁
- [x] 怎么控制去并发操作 map
  - 可以使用读写锁，在写入时 `Lock`，在读取时使用 `RLock`
- [x] 细说 go 的各种锁
  - 互斥锁：有两种工作模式，正常模式和饥饿模式，正常模式获取锁失败时阻塞协程，并将协程按照 FIFO 顺序放入等待队列；饥饿模式锁会在释放时直接传递给等待时间最长的协程，并且多次获取锁失败的协程会放在等待队列的头部
  - 读写锁见以下
- [ ] go 的读写锁怎么工作
  - xxx

#### defer `(4/6)`

- [x] 什么情况下会引发 panic
  - 主动调用、数组下标越界、空指针、类型断言失败、关闭 nil channel、过早关闭 HTTP 响应体、除以 0、关闭未初始化的 channel、重复关闭 channel、未初始化 map 直接赋值、sync 计数为负数
- [x] 如何保存程序崩溃时的数据
  - recover？
- [ ] 讲一下 defer
- [x] 子 goroutine 的 panic 会不会被父 g 捕获
  - 不会
- [x] 如何捕获子 goroutine 的 panic
  - 子协程中 recover 上报到主协程
  - https://taoshu.in/go/goroutine-panic.html
- [ ] 多个 defer 顺序，defer 什么时候有机会修改返回值
  - https://github.com/mao888/golang-guide/blob/main/golang/go-Interview/GOALNG_INTERVIEW_COLLECTION.md#4go-defer%E5%A4%9A%E4%B8%AA-defer-%E7%9A%84%E9%A1%BA%E5%BA%8Fdefer-%E5%9C%A8%E4%BB%80%E4%B9%88%E6%97%B6%E6%9C%BA%E4%BC%9A%E4%BF%AE%E6%94%B9%E8%BF%94%E5%9B%9E%E5%80%BC

#### context `(0/2)`

- [ ] 超时处理
- [ ] 优雅关闭怎么实现

#### channel `(1/9)`

- [ ] channel 长度为 0 的情况，具体讲讲
- [ ] channel 内部结构是什么 有无缓冲并发数据读写问题 底层锁
- [ ] channel 分配在堆上还是栈上的呢？
- [ ] channel 和 go（估计是想说 goroutine）联合使用有什么奇迹？
- [x] 定义长度为 0 的 channel 可以吗
  - 可以，阻塞式 channel
- [ ] 读写一个关闭的 channel 会发生什么，对未初始化的 channel 读写会怎么样
  - https://cloud.tencent.com/developer/article/1796707
  - 写会 panic，读可以正常读取剩余有效值，后续只能读出零值
- [ ] channel 有什么用？
- [ ] golang 通过通信来实现共享内存
- [ ] channel 主要使用在什么场景？

#### GC `(0/3)`

- [ ] golang 的垃圾回收，三色标记法
- [ ] 局部变量垃圾回收？GC？谈谈 GC 说了三个算法 三色回收
- [ ] 为什么 gc 会让程序变慢

#### 调度 `(0/4)`

- [ ] m 和 p 的数量关系
- [ ] GMP 个数上有什么限制吗？
- [ ] Golang 并发模型
  - https://studygolang.com/articles/10631?fr=sidebar
- [ ] go 中变量分配在什么地方
  - https://mp.weixin.qq.com/s/_Rm2FeBq2mXKykec1Un0zA

#### interface `(1/4)`

- [x] 接口的原理了解吗？
  - 空接口底层是一个 `eface` 结构，非空接口底层是 `iface` 结构
  - 给空接口赋值时会记录动态类型元数据和动态值
  - 给非空接口赋值时会从变量中拷贝定义的方法，记录动态类型元数据和动态值
- [ ] 如何复用 interface，如果用的 interface 实现 OOP 中的多态的话要怎么做？
- [ ] golang 接口，隐式实现 golang 面向对象，组合方式
- [ ] 两个 interface {} 能不能比较

#### error `(0/1)`

- [ ] 了解 Go 的错误类型吗

#### receiver `(0/1)`

- [ ] 为什么字段和方法分开写？优势是什么？知道函数接收器吗

#### 反射/unsafe `(0/3)`

- [ ] golang 中解析 tag 是怎么实现的？反射原理是什么？
  - golang 中解析 tag 是怎么实现的？反射原理是什么？
- [ ] reflect（反射包）如何获取字段 tag？为什么 json 包不能导出私有变量的 tag？
  - https://mp.weixin.qq.com/s/WK9StkC3Jfy-o1dUqlo7Dg
- [ ] 能说说 uintptr 和 unsafe.Pointer 的区别吗？
  - https://mp.weixin.qq.com/s/IkOwh9bh36vK6JgN7b3KjA

### 其他 `(0/16)`

- [ ] go 栈扩容和栈缩容，连续栈的缺点
  - https://segmentfault.com/a/1190000019570427
- [ ] 内存泄漏怎么排查？了解内存泄漏吗？在 Go 中，那些情况会引发内存泄漏
  - https://wudaijun.com/2019/09/go-performance-optimization/
  - https://colobu.com/2019/08/28/go-memory-leak-i-dont-think-so/
  - https://mp.weixin.qq.com/s/T6XXaFFyyOJioD6dqDJpFg
  - https://cloud.tencent.com/developer/article/1796711
  - https://mp.weixin.qq.com/s?__biz=MzA4ODg0NDkzOA==&mid=2247487157&idx=1&sn=cbf1c87efe98433e07a2e58ee6e9899e&source=41#wechat_redirect
  - https://mp.weixin.qq.com/s/d0olIiZgZNyZsO-OZDiEoA
- [ ] 拷贝大切片一定比小切片代价大吗
  - https://cloud.tencent.com/developer/article/1797579
- [ ] 不同包的多个 init 函数的运行时机
- [ ] go 的多路 I/O 复用
- [ ] 了解 Go 的单元测试吗
- [ ] go 的内存模型（挺难的
  - https://taoshu.in/go/memory-model.html
- [ ] 如何理解“不要通过共享内存来通信，而应该通过通信来共享内存”
- [ ] go 有几种引用类型？基本类型中哪些是值类型，哪些是指针类型
  - 指针，slice，map，chan，interface
- [ ] 变量逃逸
  - https://cloud.tencent.com/developer/article/1797578
  - https://cloud.tencent.com/developer/article/1796712
  - https://mp.weixin.qq.com/s?__biz=Mzg5NDY2MDk4Mw==&mid=2247486360&idx=1&sn=62add4f7def9638a0a1c83c1672992c5&source=41#wechat_redirect
- [ ] init 和 main 函数的执行顺序
  - 先 init 后执行 main
- [ ] go 中变量分配在什么地方
  - 在函数中分配到栈上，逃逸之后会分配到堆上（<Badge text="还有吗" type="tip"/>）
- [ ] new 和 make 区别
  - https://github.com/mao888/golang-guide/blob/main/golang/go-Interview/GOALNG_INTERVIEW_COLLECTION.md#1golang-%E4%B8%AD-make-%E5%92%8C-new-%E7%9A%84%E5%8C%BA%E5%88%AB%E5%9F%BA%E6%9C%AC%E5%BF%85%E9%97%AE
- [ ] 项目，项目设计表设计，怎么解决支付失败，原子性问题
- [ ] 有没有看过 go ws 库的源码
- [ ] 什么是写屏障、混合写屏障，如何实现？
- [ ] 怎么访问私有成员
  - unsafe、reflect

### 框架 `(0/12)`

- [ ] gin 里面的路由
- [ ] grpc 底层原理
- [ ] go 项目用了 gin 的那些模块
- [ ] gin 的参数校验，如何校验字段（这个也没太听懂，我感觉是让讲json的tag，binding之类的）
- [ ] 有了解过 Golang 有哪些现成的框架
- [ ] 为什么要做这个项目，基于什么场景
- [ ] 你觉得在你的方案中，最容易出问题的点在哪
- [ ] 一般框架都需要用到消息中间件来做一个中转，消息传递怎么做的
- [ ] rpc 调用中，客户端都发生了什么，要完成哪些工作
- [ ] rpc 客户端如何处理请求失败（如何充实）
- [ ] grpc 怎么用的
- [ ] 为什么 grpc 速度快

### 题目 `(0/4)`

- [ ] 火车站卖票，四个窗口，10000 张票，使用 golang 实现
- [ ] 开五个协程，全部执行一个函数，怎么保证协程执行完全部打印
- [ ] 使用协程交替打印字符
- [ ] 利用 golang 特性，设计一个 QPS 为 500 的服务器

## 系统/并发/TCP <Badge text="非常重要/一" type="warning"/> `(17/39)`

### [TCP](https://xiaolincoding.com/network/) `(13/23)`

- [x] [OSI 七层模型](https://www.freecodecamp.org/chinese/news/osi-model-networking-layers/)，tcp 和 udp 属于哪层
  - 应用层、表示层、会话层、传输层、网络层、数据链路层、物理层
  - tcp/udp 属于传输层
- [ ] 键入网址到网页显示，期间发生了什么？
  - xxx
- [x] 数据链路层协议
  - 以太网协议（IEEE）、IEEE802.3 协议（CSMA/CD）、PPP（点到点链路层协议）、HDLC 协议
  - 封装成帧、差错检测、可靠传输
- [ ] TCP 靠什么机制保证可靠性
  - 停止等待协议
  - 发送、接受窗口，回退 N 帧协议
  - 选择重传协议
- [x] 网络代理，网络代理正向和反向区别
  - 正向代理其实是客户端的代理, 帮助客户端访问其无法访问的服务器资源
  - 反向代理则是服务器的代理，帮助服务器做负载均衡，安全防护等
- [ ] http 协议请求和相应报文格式
- [ ] grpc 和 http 的区别
- [ ] syn 攻击
  - https://xiaolincoding.com/network/3_tcp/syn_drop.html#%E5%9D%91%E7%88%B9%E7%9A%84-tcp-tw-recycle
  - https://xiaolincoding.com/network/3_tcp/challenge_ack.html
- [x] http 状态码 HTTP 状态码 3xx、502？
  - 1xx 提示信息，协议处理的中间状态
  - 2xx 成功，报文已收到并被正确处理
    - 200 OK
    - 204 No Content 与 200 类似但是响应头没有 body 数据
    - 206 Partial Content 应用与 HTTP 分块下载或断点续传
  - 3xx 重定向，资源位置发生变动，客户端需要重新发起请求，会在响应头使用字段 Location 标识后续需要跳转的 URL
    - 301 Moved Parmanently 表示永久重定向
    - 302 Moved ？表示临时重定向
    - 304 Not Modified 不具有跳转含义表示资源未修改，重定向已存在的缓存文件，也成缓存重定向
  - 4xx 客户端错误，请求报文有误，服务器无法处理
    - 400 Bad Request 表示客户端请求有误
    - 403 Forbidden 表示服务器禁止访问资源
    - 404 Not Found 表示请求资源不存在
  - 5xx 服务器错误，服务器处理请求内部发生错误
    - 500 Internal Server Error 服务器发生了未知错误
    - 501 Not Implemented 表示客户端请求功能还不支持
    - 502 Bad Gateway 通常表示服务器作为网关或代理时返回的错误，表示服务器自身工作正常，访问后端服务器发生错误
    - 503 Service Unavailable 表示服务器正忙，无法响应
- [x] CDN
  - 内容分发网络（CDN）是指一组分布在不同地理位置的服务器，协同工作以提供互联网内容的快速交付
- [x] TCP UDP 的区别，UDP 什么时候使用
  - TCP 是面向连接的、TCP 连接只能一对一通讯、是面向字节流的、提供可靠传输服务（流量控制和拥塞控制）、首部最小 20B，最大 60B
  - UDP 是无连接的、支持多对多/一对多/一对一通信、对应用层交付的报文直接打包、不可靠传输服务、首部 8B
  - UDP 在实时性要求高的场景使用，IP 电话、视频会议等
- [x] 计算机网络层数，tcp/ip
  - OSI 七层、TCP/IP 四层
- [ ] socket、websocket 和 socket 区别
- [ ] 完整的说一下 socket 编程的底层，基于 tcp 和 udp 的都要说。这个地方甚至问了几个函数的具体参数含义
  - https://xiaolincoding.com/network/3_tcp/tcp_interview.html#%E9%92%88%E5%AF%B9-tcp-%E5%BA%94%E8%AF%A5%E5%A6%82%E4%BD%95-socket-%E7%BC%96%E7%A8%8B
- [x] tcp 三次握手是否能够减少为两次
  - 不可以，如果减少为两次，假设第一次 TCP 连接请求在网络节点中滞留导致超时重传，当与服务端建立连接并断开连接之后，服务端收到了滞留的连接请求，此时因为两次握手，服务端进入了连接已建立状态，而对客户端连接请求的确认报文因为客户端已关闭会被忽略，导致服务端一直等待客户端进程的数据
- [x] 四次挥手，解释第三次挥手 为什么三次握手四次挥手
  - 三次握手建立连接：
    - 握手前：客户端服务端都处于 `CLOSED` 状态，服务端初始化传输控制块（发送、接受指针、重传队列等），并进入 `LISTEN` 状态
    - 第一次：客户端发起连接请求 `SYN 1 seq x` 并进入同步已发送状态 `SYNC-SENT`
    - 第二次：服务端发送 TCP 连接请求确认报文 `SYN 1 ACK 1 ack x+1 seq y` 并进入同步已接收状态 `SYN-RCVD`
    - 第三次：客户端发送对服务端的连接请求确认报文的确认报文 `ACK 1 seq x+1 ack y+1` 并进入连接已建立状态 `ESTABLISHED`，服务端接收后也进入连接已建立状态
  - 四次挥手释放连接：
    - 挥手前：双方都处于连接已建立状态
    - 第一次：客户端发起 TCP 连接释放请求 `FIN 1 ACK 1 seq m ack n` 并进 `FIN-WAIT-1` 终止等待 1 状态
    - 第二次：服务端发起对客户端连接释放请求的确认 `ACK 1 seq n ack m+1` 并进入等待关闭状态 `CLOSE-WAIT`；客户端接收到该报文后就进入终止等待 2 状态 `FIN-WAIT 2`
    - 第三次：服务端发送完剩余数据后，发送 TCP 释放连接请求 `FIN 1 ACK 1 seq w ack m+1`，并进入 `LAST-ACK` 最后确认状态
    - 第四次：客户端发送对服务端的最后确认报文的确认 `ACK 1 seq m+1 ack w+1` 并进入时间等待状态 `TIME-WAIT`，服务端接收到该报文后进入 `CLOSED` 状态，客户端等待 2MSL（Maximum Segment Lifetime） 后进入 `CLOSED` 状态（客户端多等待 2MSL 为了防止最后确认报文的确认报文丢失，导致服务端重传客最后确认报文而无法关闭连接）
  - 三次握手是为了使 TCP 双方能确知对方存在、能协商参数、能对运输实体资源进行分配
  - https://xiaolincoding.com/network/3_tcp/tcp_interview.html#tcp-%E5%9F%BA%E6%9C%AC%E8%AE%A4%E8%AF%86
  - https://xiaolincoding.com/network/3_tcp/tcp_three_fin.html#tcp-%E5%9B%9B%E6%AC%A1%E6%8C%A5%E6%89%8B
- [x] tcp 状态机的切换
  - https://blog.csdn.net/genzld/article/details/85317565
  - https://baijiahao.baidu.com/s?id=1654225744653405133&wfr=spider&for=pc
- [x] [tcp 滑动窗口，拥塞控制](https://mp.weixin.qq.com/s/Tc09ovdNacOtnMOMeRc_uA)
  - https://xiaolincoding.com/network/3_tcp/tcp_feature.html#%E9%87%8D%E4%BC%A0%E6%9C%BA%E5%88%B6
  - 滑动窗口流量控制：接收方通过自己的接受窗口大小来限制发送方的发送窗口；发送方收到零窗口通知后应启动计时器定时向接收方发送零窗口探测报文
  - 拥塞控制：拥塞窗口小于慢开始门限时使用慢开始算法，大于慢开始门限时使用拥塞避免算法。当产生了超时重传，将拥塞窗口设置成 1，并修改慢开始门限为拥塞窗口的一半。当未超时时收到重复的已收到的确认，立刻重传（快重传），发送方需要收到数据时不应等到自己发送数据才捎带确认，应立即确认；即使收到失序的报文也要立刻对已收到报文发送确认报文。发送方执行了快重传后也不必重置拥塞窗口成 1，而是执行拥塞避免算法（快恢复）
- [ ] https 四次握手详细过程
- [ ] time_wait 的作用？time_wait 过多会导致什么
  - https://xiaolincoding.com/network/3_tcp/tcp_interview.html#%E4%B8%BA%E4%BB%80%E4%B9%88%E9%9C%80%E8%A6%81-time-wait-%E7%8A%B6%E6%80%81
  - https://xiaolincoding.com/network/3_tcp/tcp_interview.html#time-wait-%E8%BF%87%E5%A4%9A%E6%9C%89%E4%BB%80%E4%B9%88%E5%8D%B1%E5%AE%B3
  - https://xiaolincoding.com/network/3_tcp/tcp_interview.html#%E5%A6%82%E4%BD%95%E4%BC%98%E5%8C%96-time-wait
  -
- [x] [http 版本以及区别，https 有什么区别，公钥和私钥怎么来的](https://mp.weixin.qq.com/s/bUy220-ect00N4gnO0697A)
  ::: details
  - HTTP(1.1)
    - 优点：简单、灵活且易于扩展、应用广泛且跨平台；
    - 缺点：无状态、明文传输，无状态虽然可以减轻服务器负担，不需要额外资源记录状态信息，但是完成关联性时很麻烦（解决方法 Cookie），明文传输虽然方便调试和抓包但是内容没有隐私，容易被窃取，不验证通信双方的身份，可能被伪装，也无法验证报文完整性，可能遭篡改。
    - 性能：
      - 长连接：HTTP/1.0 每发起一个请求，就要新建一次 TCP 连接（3 次握手），做了无谓的 TCP 建立和断开，增加了通信开销；HTTP/1.1 通过长连接/持久连接的通信方式，减少了重复建立和断开的额外开销
      - 管道网络传输：HTTP/1.1 采用了长连接方式后，管道网络传输成为了可能，在一个 TCP 连接里，客户端可以发起多次请求，发送了第一个请求后，不必等其回来就可以发第二个请求，减少了整体的响应时间（但是服务器还是按顺序响应，要是前面的请求回应很慢，后面就会有许多请求排队等待，即“队头堵塞”）
    - HTTP/1.1 仍有性能瓶颈
      - 请求/响应头部（Header）未经压缩就发送，首部信息越多延迟越大。只能压缩 Body 的部分
      - 发送冗长的首部。每次互相发送相同的首部造成的浪费较多
      - 服务器是按请求的顺序响应的，如果服务器响应慢，会招致客户端一直请求不到数据，也就是队头阻塞
      - 没有请求优先级控制
      - 请求只能从客户端开始，服务器只能被动响应
  - HTTP2
    - 压缩头部：HTTP2 会压缩头（Header）如果你同时发出多个请求，他们的头是一样的或是相似的，那么，协议会帮你消除重复的部分（这就是所谓的 HPACK 算法：在客户端和服务器同时维护一张头信息表，所有字段都会存入这个表，生成一个索引号，以后就不发送同样字段了，只发送索引号，这样就提高速度了）
    - 二进制格式：HTTP/2 不再像 HTTP/1.1 里的纯文本形式的报文，而是全面采用了二进制格式，头信息和数据体都是二进制，并且统称为帧（frame）：头信息帧和数据帧。这样虽然对人不友好，但是对计算机非常友好，因为计算机只懂二进制，那么收到报文后，无需再将明文的报文转成二进制，而是直接解析二进制报文，这增加了数据传输的效率
    - 数据流：HTTP/2 的数据包不是按顺序发送的，同一个连接里面连续的数据包，可能属于不同的回应。因此，必须要对数据包做标记，指出它属于哪个回应。每个请求或回应的所有数据包，称为一个数据流（Stream）。每个数据流都标记着一个独一无二的编号，其中规定客户端发出的数据流编号为奇数，服务器发出的数据流编号为偶数。客户端还可以指定数据流的优先级。优先级高的请求，服务器就先响应该请求。
    - 多路复用：HTTP/2 是可以在一个连接中并发多个请求或回应，而不用按照顺序一一对应。移除了 HTTP/1.1 中的串行请求，不需要排队等待，也就不会再出现「队头阻塞」问题，降低了延迟，大幅度提高了连接的利用率。（例子）
    - 服务器推送：HTTP/2 还在一定程度上改善了传统的「请求 - 应答」工作模式，服务不再是被动地响应，也可以主动向客户端发送消息。（例子）
    - 缺点
      - 多个 HTTP 请求在复用一个 TCP 连接，下层的 TCP 协议是不知道有多少个 HTTP 请求的，所以一旦发生了丢包现象，就会触发 TCP 的重传机制，这样在一个 TCP 连接中的所有的 HTTP 请求都必须等待这个丢了的包被重传回来
        - HTTP/1.1 中的管道（pipeline）传输中如果有一个请求阻塞了，那么队列后请求也统统被阻塞住了
        - HTTP/2 多请求复用一个TCP连接，一旦发生丢包，就会阻塞住所有的 HTTP 请求
      - 这都是基于 TCP 传输层的问题，所以 HTTP/3 把 HTTP 下层的 TCP 协议改成了 UDP。UDP 发生是不管顺序，也不管丢包的，所以不会出现 HTTP/1.1 的队头阻塞 和 HTTP/2 的一个丢包全部重传问题
  - HTTP3：UDP 是不可靠传输的，但基于 UDP 的 QUIC 协议 可以实现类似 TCP 的可靠性传输
    - QUIC 有自己的一套机制可以保证传输的可靠性的。当某个流发生丢包时，只会阻塞这个流，其他流不会受到影响
    - TL3 升级成了最新的 1.3 版本，头部压缩算法也升级成了 QPack
    - HTTPS 要建立一个连接，要花费 6 次交互，先是建立三次握手，然后是 TLS/1.3 的三次握手。QUIC 直接把以往的 TCP 和 TLS/1.3 的 6 次交互合并成了 3 次，减少了交互次数
    - QUIC 是一个在 UDP 之上的伪 TCP + TLS + HTTP/2 的多路复用的协议
  - HTTP/HTTPS 区别：HTTP 是超文本传输协议，信息是明文传输，存在安全风险的问题。HTTPS 则解决 HTTP 不安全的缺陷，在 TCP 和 HTTP 网络层之间加入了 SSL/TLS 安全协议，使得报文能够加密传输。HTTP 连接建立相对简单， TCP 三次握手之后便可进行 HTTP 的报文传输。而 HTTPS 在 TCP 三次握手之后，还需进行 SSL/TLS 的握手过程，才可进入加密报文传输。HTTP 的端口号是 80，HTTPS 的端口号是 443。HTTPS 协议需要向 CA（证书权威机构）申请数字证书，来保证服务器的身份是可信的。
    - 总结来说：HTTPS 在 HTTP 与 TCP 层之间加入了 SSL/TLS 协议，通过信息加密、校验机制、身份证书解决了 HTTP 的窃听风险、篡改风险、冒充风险
      - 混合加密的方式实现信息的机密性，解决了窃听的风险
        - HTTPS 采用的事对称加密和非对称加密结合的混合加密方式。在通信建立前采用非对称加密的方式交换「会话秘钥」，后续就不再使用非对称加密；在通信过程中全部使用对称加密的「会话秘钥」的方式加密明文数据
        - 对称加密只使用一个密钥，运算速度快，密钥必须保密，无法做到安全的密钥交换
        - 非对称加密使用两个密钥：公钥和私钥，公钥可以任意分发而私钥保密，解决了密钥交换问题但速度慢
      - 摘要算法的方式来实现完整性，它能够为数据生成独一无二的「指纹」，指纹用于校验数据的完整性，解决了篡改的风险
        - 摘要算法用来实现完整性，能够为数据生成独一无二的「指纹」，用于校验数据的完整性，解决了篡改的风险
        - 客户端在发送明文之前会通过摘要算法算出明文的「指纹」，发送的时候把「指纹 + 明文」一同加密成密文后，发送给服务器，服务器解密后，用相同的摘要算法算出发送过来的明文，通过比较客户端携带的「指纹」和当前算出的「指纹」做比较，若「指纹」相同，说明数据是完整的
      - 将服务器公钥放入到数字证书中，解决了冒充的风险（数字证书）
        - 客户端先向服务器端索要公钥，然后用公钥加密信息，服务器收到密文后，用自己的私钥解密
        - 第三方权威机构 CA（数字证书认证机构），将服务器公钥放在数字证书（由数字证书认证机构颁发）中，只要证书是可信的，公钥就是可信的。通过数字证书的方式保证服务器公钥的身份，解决冒充的风险
    - SSL/TLS 协议基本流程：客户端向服务器索要并验证服务器的公钥；双方协商生产「会话秘钥」；双方采用「会话秘钥」进行加密通信
      - 前两步也就是 SSL/TLS 的建立过程，也就是握手阶段，涉及四次通信
  :::
- [x] 一个 MTU 最大是 1500 字节，那么最多包含多少的数据 ip 头部字节大小为 20~60，最多数据即 1500-20=1480
- [ ] DNS 协议，详细过程
  - 递归、迭代

### 系统/并发 `(4/16)`

- [ ] linux 中线程的状态
- [x] 如何终止一个运行中的线程
  - kill
- [x] 为什么要区分用户态和内核态
  - 内核具有很高的权限，可以控制 cpu、内存、硬盘等硬件，而应用程序具有的权限很小，因此大多数操作系统，把内存分成了两个区域：内核空间，这个内存空间只有内核程序可以访问；用户空间，这个内存空间专门给应用程序使用；
- [ ] 网络 io 模型
  - https://xiaolincoding.com/os/8_network_system/selete_poll_epoll.html
- [ ] 用户态和内核态的区别
- [ ] 锁的概念
- [x] 死锁及如何避免死锁
  - 当两个线程为了保护两个不同的共享资源而使用了两个互斥锁，那么这两个互斥锁应用不当的时候，可能会造成两个线程都在等待对方释放锁，在没有外力的作用下，这些线程会一直相互等待，就没办法继续运行，这种情况就是发生了死锁（满足：互斥条件-多个线程不能同时使用同一个资源；持有并等待条件-线程在等待其他资源的同时并不会释放自己已经持有的资源；不可剥夺条件-在自己使用完之前不能被其他线程获取；环路等待条件-两个线程获取资源的顺序构成了环形链）
  - 破坏其中一个条件，常用方法是使用资源有序分配法，来破环环路等待条件
- [ ] i/o 多路复用技术是完美的吗？
- [x] 上下文切换到底切换了什么？
  - 进程的上下文切换不仅包含了虚拟内存、栈、全局变量等用户空间的资源，还包括了内核堆栈、寄存器等内核空间的资源
  - 当两个线程是属于同一个进程，因为虚拟内存是共享的，所以在切换时，虚拟内存这些资源就保持不动，只需要切换线程的私有数据、寄存器等不共享的数据
- [ ] 内存分页、分段
  - https://xiaolincoding.com/os/3_memory/vmem.html#%E5%86%85%E5%AD%98%E5%88%86%E6%AE%B5
  - https://xiaolincoding.com/os/3_memory/vmem.html#%E5%86%85%E5%AD%98%E5%88%86%E9%A1%B5
- [ ] os 内存伙伴算法
- [ ] 生产者消费者问题
  - https://xiaolincoding.com/os/4_process/multithread_sync.html#%E7%94%9F%E4%BA%A7%E8%80%85-%E6%B6%88%E8%B4%B9%E8%80%85%E9%97%AE%E9%A2%98
- [ ] 有一台 linux 服务器，上面需要跑 Redis、Mysql、Web 三个服务，你会如何对这台 linux 服务器做性能测试和验收测试，会使用到哪些工具，关注哪些问题？
  - https://xiaolincoding.com/os/9_linux_cmd/linux_network.html
  - https://xiaolincoding.com/os/9_linux_cmd/pv_uv.html#uv-%E5%88%86%E7%BB%84
- [ ] 十进制和二进制怎么转换
- [ ] 什么是一致性哈希？
  - https://xiaolincoding.com/os/8_network_system/hash.html
- - [x] 虚拟内存怎么关联到物理内存的？
  - 操作系统引入了虚拟内存，进程持有的虚拟地址会通过 CPU 芯片中的内存管理单元（MMU）的映射关系，来转换变成物理地址，然后再通过物理地址访问内存
  - 内存分段和内存分页

## 数据库 <Badge text="非常重要/一" type="warning"/> `(7/11)`

- [ ] mysql 的存储引擎了解的有哪些
  - https://www.cnblogs.com/andy6/p/5789248.html
- [ ] 分布式的 cap 理论
- [ ] mysql 和 mongodb 区别
- [ ] mysql b+ 树索引和 hash 索引的区别
- [x] sql 语句发现运行慢，如何优化
  - 前缀索引优化
  - 覆盖索引优化
  - 主键索引最好是自增的
  - 防止索引失效
- [x] 你怎么知道它（一条 SQL 语句）使用没索引呢？（SHOW INDEX / EXPLAN）
  - key key_len
- [x] 向数据库里读写数据的流程是什么样子？
  - 连接器：建立连接，管理连接、校验用户身份
  - 查询缓存：查询语句如果命中查询缓存则直接返回，否则继续往下执行。MySQL 8.0 已删除该模块
  - 解析 SQL，通过解析器对 SQL 查询语句进行词法分析、语法分析，然后构建语法树，方便后续模块读取表名、字段、语句类型
  - 执行 SQL
    - 预处理阶段：检查表或字段是否存在；将 `select *` 中的 * 符号扩展为表上的所有列
    - 优化阶段：基于查询成本的考虑， 选择查询成本最小的执行计划
    - 执行阶段：根据执行计划执行 SQL 查询语句，从存储引擎读取记录，返回给客户端
- [x] 讲讲 mysqlMVCC
  - Read View 中四个字段：creator_trx_id、m_ids、min_trx_id、max_trx_id
  - 聚簇索引记录中两个跟事务有关的隐藏列：trx_id、roll_pointer
  - 通过「版本链」来控制并发事务访问同一个记录时的行为就叫 MVCC（多版本并发控制）
- [x] 什么是幻读？
- [x] 了解覆盖索引吗？什么时候用覆盖索引？
  - query 的所有字段在二级索引的 B+Tree 上都能找到记录不需要再通过聚簇索引执行回表操作
- [x] mysql 的 ACID
  - 事务特性：原子性（undo log 回滚日志）、一致性（持久性+原子性+隔离性）、隔离性（MVCC）、持久性（redo log 重做日志）
  - 并行事务问题：脏读、不可重复度、幻读
  - 隔离级别：读未提交、读提交、可重复读、串行化

## Redis <Badge text="非常重要/一" type="warning"/> `(7/19)`

redis 未查到，数据也未查到需要记录 key 防止多次穿透

- [ ] 讲一下 Redis 集群高可用、主从复制的理解
  - https://xiaolincoding.com/redis/cluster/master_slave_replication.html
- [ ] redis 做补偿的时候挂了怎么办
- [ ] redis 数据结构 用在哪些场景？说一下五种 redis 数据结构和之间的实现方式
  - https://xiaolincoding.com/redis/data_struct/data_struct.html
  - https://xiaolincoding.com/redis/data_struct/command.html
  - https://xiaolincoding.com/redis/base/redis_interview.html#redis-%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84
- [ ] 热 key 问题的解决
- [ ] 哨兵
- [ ] redis 和数据库数据不一致（延迟双删 binlog）
  - https://xiaolincoding.com/redis/cluster/master_slave_replication.html#%E5%A6%82%E4%BD%95%E5%BA%94%E5%AF%B9%E4%B8%BB%E4%BB%8E%E6%95%B0%E6%8D%AE%E4%B8%8D%E4%B8%80%E8%87%B4
- [ ] redis 秒杀场景
  - https://xiaolincoding.com/cs_learn/feel_cs.html#%E9%AB%98%E5%B9%B6%E5%8F%91%E6%9E%B6%E6%9E%84%E7%9A%84%E8%AE%BE%E8%AE%A1
- [ ] Redis 这块，如果你作为一个 Redis 管理者，对使用有什么建议吗
- [ ] redis 的缓存双写一致性你如何保证
  - https://xiaolincoding.com/redis/architecture/mysql_redis_consistency.html
  - https://xiaolincoding.com/redis/base/redis_interview.html#redis-%E7%BC%93%E5%AD%98%E8%AE%BE%E8%AE%A1
- [ ] 选一个常用类型说一下底层实现
- [ ] redis 跳表、动态字符串
- [ ] redis 的过期时间 TTL，是谁来负责更新的？就比如过期时间是 100，是什么负责把它更新为 99 呢？
- [x] redis 惊群效应
- [x] redis 的内存回收
  - 内存淘汰策略共有八种，分为「不进行数据淘汰」和「进行数据淘汰」两类策略
    - 不进行数据淘汰的策略（noeviction）
    - 进行数据淘汰的策略，分为「在设置了过期时间的数据中进行淘汰」和「在所有数据范围内进行淘汰」这两类策略
      - 过期时间的数据中进行淘汰
        - volatile-random：随机淘汰设置了过期时间的任意键值
        - volatile-ttl：优先淘汰更早过期的键值
        - volatile-lru：淘汰所有设置了过期时间的键值中，最久未使用的键值
        - volatile-lfu：淘汰所有设置了过期时间的键值中，最少使用的键值
      - 在所有数据范围内进行淘汰
        - allkeys-random：随机淘汰任意键值
        - allkeys-lru：淘汰整个键值中最久未使用的键值
        - allkeys-lfu：淘汰整个键值中最少使用的键值
- [x] redis 怎么知道这个 key 已经过期了（过期字典「惰性删除+定期删除」）
  - 被动过期 尝试去访问一个过期了的 key，此时这个 key 会被删除
  - 主动过期 https://redis.io/commands/expire/
    - Redis 会定期 <Badge text="TODO" type="tip"/> 的在设置了过期时间的 key 中随机挑选测试一些 key，已过期的 key 删除
    - Redis 每秒会执行 10???? 次下面的步骤
      - 在设置了过期时间的 key 中随机挑选 20 个 key 测试
      - 删除所有已过期的 key
      - 如果有超过 25% 的 key 过期，重复第一步
        - 概率算法，假设样本代表整个 key space，Redis 继续过期直到可能过期的 key 百分比低于 25％
        - 在任意给定时刻，使用内存的已过期 key 的最大数量最大等于每秒最大写入操作数量除以 4。
- [x] redis 的分布式锁你了解多少
  - 加锁包括了读取锁变量、检查锁变量值和设置锁变量值 SETNX
  - 锁变量需要设置过期时间
  - 锁变量的值需要能区分来自不同客户端的加锁操作
  - Redlock
- [x] redis 持久化机制
  - AOF（AOF 重写机制、AOF 重写缓冲区、AOF 缓冲区、AOF 重写子进程）
  - RDB（save、bgsave）
- [x] redis 缓存穿透、击穿和雪崩以及对应的解决方案
  - 穿透：数据既不在缓存中，也不在数据库中时有大量访问
    - 限制非法请求
    - 缓存空值或者默认值
    - 布隆过滤器 TODO
  - 击穿：热点数据过期
    - 不给热点数据设置过期时间，由后台异步更新缓存，或者在热点数据准备要过期前，提前通知后台线程更新缓存以及重新设置过期时间
    - 互斥锁方案，保证同一时间只有一个业务线程更新缓存，未能获取互斥锁的请求，要么等待锁释放后重新读取缓存，要么就返回空值或者默认值
  - 雪崩：大量缓存数据在同一时间过期（失效）或者 Redis 故障宕机，全部请求都直接访问数据库
    - 大量数据同时过期：均匀设置过期时间、互斥锁、双 key 策略、后台更新缓存（类似缓存预热）
    - Redis 故障宕机：服务熔断、请求限流、构建 Redis 集群
- [x] redis 为什么更快，持久化方式，redis 单线程在多核机器里使用会不会浪费机器资源
  - 基于内存的数据库，对数据的读写操作都是在内存中完成，因此读写速度非常快
  - AOF 日志：每执行一条写操作命令，就把该命令以追加的方式写入到一个文件里；RDB 快照：将某一时刻的内存数据，以二进制的方式写入磁盘；混合持久化方式：Redis 4.0 新增的方式，集成了 AOF 和 RBD 的优点
  - CPU 并不是制约 Redis 性能表现的瓶颈所在，更多情况下是受到内存大小和网络I/O的限制

## 算法 <Badge text="非常重要/一" type="warning"/> `(11/34)`

- [ ] 二叉树和 B+ 树
- [x] 快排怎么实现
  - 双指针，分治
- [x] 熟悉哪些数据结构？
  - 排序
    - 冒泡、选择、插入；归并排序、快排序；堆排序
  - 二叉搜索树、平衡二叉树
  - ...
- [ ] 最长递增子序列
- [x] 两个 list 求相同元素
  - 排序后双指针法
  - 使用 map
- [x] 有两个有限队列，求两个队列的相同元素
  - 使用 map
  - 排序后双指针法
- [ ] 给定一个数 n，如 23121；给定一组数字 A 如 {2,4,9}，求由 A 中元素组成的、小于 n 的最大数。如小于 23121 的最大数为 22999
- [ ] 给定一个二叉树，请计算节点值之和最大的路径的节点值之和是多少。这个路径的开始节点和结束节点可以是二叉树中的任意节[ ] 点
- [x] 二分查找
  - 排序后从数组中间区分查询左部分或右部分；递归
- [ ] 字符串中最长不重复字符的子字符串
- [x] 有序数列合并
  - 双指针法
- [ ] 青蛙跳楼梯
- [ ] 反转链表
- [ ] 跳表
  - https://xiaolincoding.com/redis/data_struct/data_struct.html#%E8%B7%B3%E8%A1%A8
- [ ] 单向链表倒数第 K 个节点
- [x] 数组、链表的区别，适用的场景
  - 数组查询的时间复杂度是 o(1)，适合插入少，读多的场景
  - 链表插入的事件复杂度最坏是 o(n)，适合插入多的场景
- [ ] 哈希算法，列举几种 X
- [ ] 实现 LRU
- [ ] 平衡二叉树查找第 k 大的节点
- [ ] 拓扑图求路径数，输入一个二维数组表示图，输出从起始点到终点可能的路径数
- [ ] 九键手机 输入是只包含 1-9 的字符串，输出是对应按键可能的组合
- [ ] 链表查找倒数第 k 个节点 追问：怎么把链表线性遍历提速
- [ ] 二叉树多个节点的最近公共祖先
- [ ] 二叉树两个节点的共同父节点
- [ ] lru 算法
- [ ] 十万个数中找出最大 1000 个数
- [x] 二叉树层序遍历
  - 使用队列，遍历左右孩子后放入队列，循环直到队列为空
- [ ] 判断回文字符串
- [ ] 复杂度判断一个数是否为质数
- [ ] 第 K 大
- [x] 一组有序可重复数组找到某个最后出现的数的索引
  - 使用 map，遍历每次更新数的最新出现索引
- [x] 寻找 K 个高频的数字
  - 遍历使用 map 记录出现次数同时记录最高频
- [ ] 实现双向链表，根据索引插入节点
- [x] 给定一个有序数组和一个数字，需要你判断出这个数字在数组里的出现次数，要求时间复杂度为 $log2$

## 消息队列 <Badge text="重要/一" type="info"/> `(0/3)`

- [ ] 消息队列发生阻塞怎么处理
- [ ] 如何保证消息不丢失
- [ ] 用过什么消息队列？ RocketMQ 和 Kafka 选型的区别

## 其他 <Badge text="重要/次" type="info"/> `(0/31)`

- [ ] 现在有一个项目已经上线了不能关闭，但是监控到它的内存一直在减少，你会如何排查这个问题
- [ ] 你谈到自己比较喜欢钻研，擅长解决疑难杂症，有没有一个case分享一下
- [ ] 系统设计：微信发红包抢红包 追问：发红包和抢红包实现接口注意事项，怎么保证抢红包不超过既定数量
- [ ] 如何保证并发，考虑哪些并发控制策略，加锁性能降低怎么办
- [ ] 服务治理
- [ ] 如何限流，工作中怎么做的，具体实现？
- [ ] 分布式服务接口的幂等性如何设计
- [ ] 接口设计原则
- [ ] 数据侧为什么要分库分表
- [ ] 集群分布式 session 如实现
- [ ] CAS 是怎么一种同步机制
- [ ] 多线程为什么会带来性能问题
- [ ] 有哪几种锁。有什么特点，有什么应用场景，共享锁排它锁 乐观锁悲观锁？
- [ ] 如何解决死锁
- [ ] 如何优化目前的系统
- [ ] 负载均衡机制
- [ ] 项目体量有多大，有多少张表，项目的难点在哪里
- [ ] cookie+session和jwt的区别
- [ ] 令牌桶怎么实现的
- [ ] 雪花算法原理
- [ ] 五种IO模型
- [ ] 计网：osi 七层体系+各层协议介绍，ip 地址分类/主机号及意义，重点讲讲应用层和传输层了解的协议及其作用和特点
- [ ] Linux：讲讲文件系统，写过什么脚本，如何查看磁盘/内存占用，grep 用法，crontab 5 个*含义，定时任务怎么写的
- [ ] Docker：容器定义与实现原理，和虚拟机的区别，打包到部署的全流程，dockfile 参数含义，copy 和 add 区别，如何优化镜像大小
  - https://blog.csdn.net/weixin_38499215/article/details/101480322?spm=1001.2014.3001.5506
- [ ] 讲讲架构和技术细节，写的 API 的业务逻辑是什么，主要用过哪些数据库，为什么用 docker 进行交付，交付的具体形式是什么，k8s 用过吗，容器内定时脚本/宿主机定时脚本启动容器，监控服务的脚本怎么写的，如何完成团队协作，遇到了什么困难，如何解决的
- [ ] 如何定位问题，链路追踪
- [ ] JWT 和 oauth2 的区别 oauth2 的授权过程 为什么要使用授权码 使用了 https 还有必要使用授权码吗
- [ ] git 中 rebase 和 merge 的区别，什么时候用
- [ ] ci/cd，是在 gitlab 的仓库中使用么？
- [ ] nginx 新配置 ip 重新 run了 之前的功能也不会受到影响 不停止的配置变更 是怎么实现的（nginx 热重载问题）
- [ ] ping 原理 curl 是什么，和 ping 的区别

## Reference

- [硬核！30 张图解 HTTP 常见的面试题](https://mp.weixin.qq.com/s/bUy220-ect00N4gnO0697A)
- [HTTP 常见面试题](https://xiaolincoding.com/network/2_http/http_interview.html)
- [字节跳动日常实习面经](https://yusart.xyz/archives/%E5%AD%97%E8%8A%82%E8%B7%B3%E5%8A%A8%E6%97%A5%E5%B8%B8%E5%AE%9E%E4%B9%A0%E9%9D%A2%E7%BB%8F)
- [Go 语言设计与实现](https://draveness.me/golang/)
- [golang-guide](https://github.com/mao888/golang-guide/blob/main/golang/go-Interview/GOALNG_INTERVIEW_COLLECTION.md)
- [Golang 内存组件之 mspan、mcache、mcentral 和 mheap 数据结构](https://segmentfault.com/a/1190000039815122)
- [Go 程序员面试笔试宝典](https://golang.design/go-questions/sched/what-is/)
- [幼麟实验室：Mutex 秘籍](https://www.bilibili.com/video/BV15V411n7fM/)
- [幼麟实验室：Context了解下](https://www.bilibili.com/video/BV19K411T7NL/)

[^StringHeader]:

    ```go
    type StringHeader struct {
      Data uintptr
      Len  int
    }
    ```

[^SliceHeader]:

    ```go
    type SliceHeader struct {
      Data uintptr
      Len  int
      Cap  int
    }
    ```
