---
timeline: false
article: false
---

笔试 就是计算机基础知识：计算机网络、数据结构、操作系统、Linux、MySQL


编程题的话，也就是 模拟、动态规划、图论、回溯、二叉树，遇到过一些LeetCode原题，如：无重复字符的最长子串、爱吃香蕉的珂珂、二叉树的最近公共祖先、验证回文串

其余科目问题：

1. 项目架构
2. redis秒杀场景
3. 负载均衡
4. 如何定位问题，链路追踪
5. 如何优化
6. 优雅关闭怎么实现
7. channel管道
8. context
9. 排序算法及具体细节
10. 二叉树和B+树
11. 二叉树后序遍历手撕
12. 进程，线程，协程
13. 协程适用什么场景
14. 计算机网络 层数，tcp/ip

第一个、分布式锁当一个请求来访问接口时候，拿到锁之后，去操作数据库，然后此时锁过期了，那么其它请求也会进入接口中，就会造成并发性问题，该怎么解决？
第二个、令牌桶限流，什么时候对令牌进行归还？  第三个、类似LeetCode的每日提交图，你怎么给前端响应，让前端展示

讲一下研究生阶段的研究方向
讲一下项目（针对项目，问如何做参数校验，jwt讲一下，中间件如何实现，jwt 签名是明文传输吗，前缀树如何实现）
说说常见状态码
分布式锁，延迟队列，跳表的一些问题
http request 都包含哪些信息
讲一下 HTTPS 加密过程
TCP 和 UDP 的区别讲一下
TCP 三次握手讲一下，为什么断开连接是四次挥手，比建立连接多一次，time-wait 状态作用是什么
流量控制 和 拥塞控制
数据库索引如何优化？
MySQL 事务特性
索引是有序的好，还是无序的好？（这个问题我不是很理解）
数据库都有哪些隔离级别
讲讲 MVCC，读已提交和可重复读分别如何实现
快照读 和 当前度 的区别是什么
having 和 where 分别用在哪里？having 可以替代 where 吗
了解RPC吗？讲一下
Put 和 Patch 的区别
操作系统有哪些页面置换算法
Redis 一致性hash算法

https://fitzbogard-blog-demo.vercel.app/about-me#cb18894d5c6245fca1d65531c4824339
2021 小林实习面经 （更新ing）https://talkgo.org/t/topic/1839
腾讯一面 https://talkgo.org/t/topic/1866
快手面试 https://talkgo.org/t/topic/1854
- 字节 米哈游 富途 猿辅导 https://jiekun.dev/posts/2023-interviews/
  - https://jiekun.dev/posts/2022-bytedance-interview/
  - https://jiekun.dev/posts/2023-futu-interview/
  - Go程序员面试笔试宝典 https://book.douban.com/subject/35871233/
  - https://zhuanlan.zhihu.com/p/588988298
  - https://yusart.xyz/archives/%E5%AD%97%E8%8A%82%E8%B7%B3%E5%8A%A8%E6%97%A5%E5%B8%B8%E5%AE%9E%E4%B9%A0%E9%9D%A2%E7%BB%8F

360 — 服务端开发工程师-Golang
奇安信 — 服务端开发工程师-Golang
滴滴 — 后端研发工程师（Golang/Java/C++/PHP）
小米 — 软件开发工程师-Golang方向
快手 — Golang 开发工程师
完美世界 — C++/Golang开发工程师
蚂蚁集团 — 研发工程师Golang/Python
深信服 — Go语言开发工程师
大华 — 【研发中心】2023届Golang开发工程师(J17977)
得物APP — Golang开发工程师（上海）
美团 — 后端开发工程师
祖龙娱乐 — Go游戏服务器开发工程师（北京）
希望学 — 服务端Golang开发工程师
高仙机器人 — 后端开发工程师-Golang方向
百度 — 北京-C++/PHP/Go研发工程师(J48055)
斗鱼 — 服务端开发工程师（Golang）
海能达 — Golang软件工程师
同花顺 — Golang开发工程师-认证中心
字节跳动 — 后端开发工程师
闪送 — Golang开发工程师（运力体系）
中国人寿 — 软件开发岗-Golang方向
联想 — Golang开发工程师-Lenovo Research
红海无限 — 后端开发工程师
Aibee — 后端开发工程师-Golang
58集团 — 后端开发工程师-北京
兴业数金 — Golang开发工程师
欧科云链 — Golang开发工程师
高途 — Golang开发工程师
小丑鱼 — 后端开发工程师
北森 — 后端开发工程师
孩子王 — Golang 开发工程师
