---
title: 69. x 的平方根
problem_no: 69
date: 2021-10-09
description: 
timeline: false
article: false
---

<!-- Description. -->

<!-- more -->

## Problem

Source: [LeetCode 69](https://leetcode-cn.com/problems/sqrtx/){target="_blank"}

### Description

给你一个非负整数 `x` ，计算并返回 `x` 的 **算术平方根** 。

由于返回类型是整数，结果只保留 **整数部分** ，小数部分将被 **舍去** 。

注意：不允许使用任何内置指数函数和算符，例如 `pow(x, 0.5)` 或者 `x ** 0.5` 。

示例 1：

```text
输入：x = 4
输出：2
```

示例 2：

```text
输入：x = 8
输出：2
解释：8 的算术平方根是 2.82842..., 由于返回类型是整数，小数部分将被舍去。
```

提示：

- $0 <= x <= 2^31 - 1$

## Solution

## Code

@[code cpp](../../_codes/algorithm/code/leet-code/69-main.cpp)