---
title: 20. 有效的括号
problem_no: 20
date: 2021-09-25
description: 
timeline: false
article: false
---

<!-- Description. -->

<!-- more -->

## Problem

Source: [LeetCode 20](https://leetcode-cn.com/problems/valid-parentheses/){target="_blank"}

### Description

给定一个只包括 `'('`，`')'`，`'{'`，`'}'`，`'['`，`']'` 的字符串 `s` ，判断字符串是否有效。

有效字符串需满足：

- 左括号必须用相同类型的右括号闭合。
- 左括号必须以正确的顺序闭合。

示例 1：

```text
输入：s = "()"
输出：true
```

示例 2：

```text
输入：s = "()[]{}"
输出：true
```

示例 3：

```text
输入：s = "(]"
输出：false
```

示例 4：

```text
输入：s = "([)]"
输出：false
```

示例 5：

```text
输入：s = "{[]}"
输出：true
```

提示：

- $1 <= s.length <= 10^4$
- `s` 仅由括号 `'()[]{}'` 组成

## Solution

## Code

@[code cpp](../../_codes/algorithm/code/leet-code/20-stack.cpp)