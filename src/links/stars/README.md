# 收藏

## 优质博客

::: projects

```yaml
- icon: https://files.codelife.cc/icons/zhongguose.com.svg
  name: 涛叔
  desc: 乐乎
  link: https://taoshu.in/
- icon: https://image.liubing.me/2023/02/05/834597e9e927e.png
  name: 小林
  desc: 图解计算机基础
  link: https://xiaolincoding.com
- icon: https://avatars.githubusercontent.com/u/230646?v=4?v=3&s=88
  name: Yukang
  desc: 程序员的喵
  link: https://catcoding.me/
- icon: https://avatars.githubusercontent.com/u/230646?v=4?v=3&s=88
  name: 陈皓
  desc: 酷壳
  link: https://coolshell.cn/
- icon: https://cdn.alomerry.com/blog/img/links/me.svg
  name: Xiaohan Zou
  desc: 邹笑寒
  link: https://zxh.io
- icon: https://image.liubing.me/2023/02/05/834597e9e927e.png
  name: 二丫讲梵
  desc: 二丫讲梵
  link: https://wiki.eryajf.net/comein/
- icon: https://image.liubing.me/2023/02/05/834597e9e927e.png
  name: 柳婼 の blog
  desc: 我不管，反正我最萌～
  link: https://www.liuchuo.net
- icon: https://theme-hope.vuejs.press/logo.png
  name: VuePress Theme Hope
  desc: 一个具有强大功能的 vuepress 主题✨
  link: https://theme-hope.vuejs.press/zh/
- icon: https://theme-hope.vuejs.press/logo.png
  name: 幼麟实验室
  desc: 做点儿形象通透的编程教程。
  link: https://www.zhihu.com/people/kylin-lab/
- icon: https://www.luozhiyun.com/favicon.ico
  name: luozhiyun`s Blog
  desc: xxx
  link: https://www.luozhiyun.com/
```

:::

https://xargin.com/page/4/
为什么要旗帜鲜明地反对 orm 和 sql builder
https://xargin.com/you-should-avoid-orm-and-sql-builder/


- https://blognas.hwb0307.com/

- 音乐解锁 https://demo.unlock-music.dev/

## Something

::: projects

```yaml
- icon: https://files.codelife.cc/icons/zhongguose.com.svg
  name: 中国色
  desc: 中国传统颜色，提供 RGB、HEX 总有一款惊艳你
  link: http://zhongguose.com/
- icon: https://image.liubing.me/2023/02/05/834597e9e927e.png
  name: lottiefiles
  desc: lottiefiles
  link: https://lottiefiles.com/
- icon: https://image.liubing.me/2023/02/05/834597e9e927e.png
  name: JSON Sorter
  desc: json 排序
  link: https://codeshack.io/json-sorter/
- icon: https://image.liubing.me/2023/02/05/834597e9e927e.png
  name: Squoosh
  desc: google 图片压缩
  link: https://squoosh.app/
- icon: https://image.liubing.me/2023/02/05/834597e9e927e.png
  name: Waifulabs
  desc: 二次元头像生成
  link: https://waifulabs.com/generate
- icon: https://cdn.alomerry.com/blog/img/links/vuepress.svg
  name: GitHub Proxy
  desc: GitHub 文件代理免费加速下载服务
  link: https://ghproxy.com/
- icon: https://files.codelife.cc/icons/zhongguose.com.svg
  name: Excalidraw
  desc: 虚拟白板，用于绘制手绘风格的图表。
  link: https://excalidraw.com/
```

:::

## 工具

双拼练习 https://api.ihint.me/shuang/

https://webgradients.com/

- REAMD 徽标网站
  - https://badgen.net/
  - https://shields.io/

- icon: https://image.liubing.me/2022/12/30/c827badf9fa7a.jpg
  name: iconfont
  desc: 阿里巴巴矢量图标库。
  link: https://www.iconfont.cn/

## 指南

https://github.com/byoungd/English-level-up-tips https://www.v2ex.com/t/425450

## gpt

- https://openai.apifox.cn/api-67883981
- https://github.com/pengzhile/pandora
- https://github.com/xx025/carrot
- https://www.v2ex.com/t/936590#reply11
