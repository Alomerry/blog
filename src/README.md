---
home: true
layout: BlogHome
icon: home
title: 主页
# heroImage: https://cdn.alomerry.com/blog/avatar.png
heroImageStyle: "border-radius: 50%;"
heroText: 时日无多
tagline: Keep Working And Never Give Up！
bgImage: https://api.kdcc.cn/
heroFullScreen: false
projects:
  - icon: cib:leetcode
    name: 算法题解
    desc: PAT、LeetCode 题解
    link: /ioi/
  - icon: link
    name: 收藏
    desc: 一些博客、工具
    link: /links/stars/
  - icon: book
    name: 美文文摘
    desc: 读者、文苑、知乎、微博等
    link: /space/digest
  - icon: article
    name: 文档
    desc: 常用文档汇总
    link: /links/docs/
  # - icon: project
  #   name: 伙伴名称
  #   desc: 伙伴详细介绍
  # #   link: https://你的伙伴链接
  # - icon: 优秀博客
  #   name: 自定义项目/logo.svg
  #   desc: 自定义详细介绍
  #   link: /links/friends/

footer: '<a href="http://beian.miit.gov.cn/" rel="noopener noreferrer" target="_blank">备案号: 苏ICP备19037502号-3</a> | <a href="/about/">关于网站</a>'
---