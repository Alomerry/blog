---
article: false
title: Gin Web Framework
---

## Model

### 控制器

#### 数据绑定模型

Gin 提供两套绑定方法：

- Must Bind
  - Bind、BindJson、BindXML、BindQuery、BindYAML
- Should Bind
  - ShouldBind、ShouldBindJson、ShouldBindXML、ShouldBindQuery、ShouldBindYAML


https://www.flysnow.org/2020/06/28/golang-gin-middleware.html

