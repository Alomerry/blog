---
date: 2023-08-02
timeline: false
article: false
---

- https://docs.gitlab.cn/jh/ci/variables/index.html#%E4%BD%BF%E7%94%A8-bashsh-%E5%92%8C%E7%B1%BB%E4%BC%BC%E7%9A%84%E5%8F%98%E9%87%8F
- https://docs.gitlab.cn/jh/ci/pipeline_editor/index.html
- https://docs.gitlab.cn/jh/ci/caching/
- https://www.php.cn/faq/505025.html
- https://forum.gitlab.com/t/ci-cd-pipeline-get-list-of-changed-files/26847/25
- https://forum.gitlab.com/t/ci-cd-pipeline-get-list-of-changed-files/26847/11
- https://meigit.readthedocs.io/en/latest/gitlab_ci_.gitlab-ci.yml_detail.html
