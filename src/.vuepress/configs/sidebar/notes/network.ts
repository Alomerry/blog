import { arraySidebar } from "vuepress-theme-hope";

export const network = arraySidebar([
  "transport-layer",
  "application-layer",
  "datalink-layer",
  "internet-layer",
  "physical-layer",
]);
