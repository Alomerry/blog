import { arraySidebar } from "vuepress-theme-hope";

export const jenkins = arraySidebar([
  "",
  "install-and-use",
  "pipeline",
  "plugins"
]);