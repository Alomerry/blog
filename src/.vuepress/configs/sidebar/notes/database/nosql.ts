import { arraySidebar } from "vuepress-theme-hope";

export const nosql = arraySidebar([
  "",
  "redis",
  "mongodb"
]);