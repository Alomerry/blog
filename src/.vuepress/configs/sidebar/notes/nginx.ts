import { arraySidebar } from "vuepress-theme-hope";

export const nginx = arraySidebar([
  "nginx",
  "nginx-mirror",
  "nginx-rtmp",
]);
