# Alomerry Wu's Blog

[![Build Status](https://ci.alomerry.com/buildStatus/icon?job=vuepress-blog)](https://ci.alomerry.com/job/vuepress-blog/)
[![License](https://img.shields.io/static/v1?label=License&message=MIT&color=red)](./LICENSE)
[![Build](https://uptime.alomerry.com/api/badge/7/ping/24?color=pink)](https://uptime.alomerry.com/status/dashboard)
[![Build](https://uptime.alomerry.com/api/badge/7/upTime/24h?color=green)](https://uptime.alomerry.com/status/dashboard)

[![VuePress2](https://img.shields.io/static/v1?logo=vuedotjs&color=blue&label=VuePress2&message=2.0.0-beta.67)](https://v2.vuepress.vuejs.org/zh/)
[![VuePress-Theme-Hope](https://img.shields.io/static/v1?logo=appveyor&color=blue&label=VuePress-Theme-Hope&message=2.0.0-beta.237)](https://theme-hope.vuejs.press/zh/)


## local git hook && oss pusher

cd blog/.vuepress
./ossPusher --configPath core.toml

==TODO==

添加 sync，从 oss_hash 下载不存在的文件到本地

## import code
